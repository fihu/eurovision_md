# Niederlande
🇬🇧 *Netherlands*
🇫🇷 *les Pays-Bas*
# Teilnahmen
[[1956]] - Jetty Paerl, "De vogels van Holland" 🇳🇱 (*dt. Die Vögel von Holland*) & Corry Brokken, "Voorgoed voorbij" 🇳🇱 (*dt. Für immer weg*) - Platz 2
**[[1957]] - Corry Brokken, "Net als toen" 🇳🇱 (*dt. Genau wie damals*) -  Platz 1**
# Erfolge
[[1957]] - Platz 1
# Austragungen
[[1958]] - 