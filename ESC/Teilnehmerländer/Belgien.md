# Belgien
🇬🇧 *Belgium*
🇫🇷 *Belgique*
# Teilnahmen
[[1956]] - Fud Leclerc, "Messieurs les noyés de la Seine" 🇫🇷 (*dt. Ihr ertrunkenen Herren der Seine*) & Mony Marc, "Le plus beau jour de ma vie" 🇫🇷 (*dt. Der beste Tag meines Lebens*) - Platz 2
[[1957]] - Bobbejaan Schoepen, "Straatdeuntje" 🇳🇱 (*dt. Straßenmelodie*) - Platz 8
# Erfolge
[[1956]] - Platz 1
# Austragungen
[[1956]] - Lugano