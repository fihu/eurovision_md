# Schweiz
🇬🇧 *Switzerland*
🇫🇷 *la Suisse*
# Teilnahmen
**[[1956]] - Lys Assia, "Refrain" 🇫🇷 (*dt. Refrain*) & Lys Assia, "Das alte Karussell" 🇩🇪 - Platz 1**
[[1957]] - Lys Assia, "L'enfant que j'étais" 🇫🇷 (*dt. Das Kind, das ich war*) -  Platz 5
# Erfolge
[[1956]] - Platz 1
# Austragungen
[[1956]] - Lugano