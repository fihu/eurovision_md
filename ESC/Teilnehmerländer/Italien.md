# Italien
🇬🇧 *Italy*
🇫🇷 *Italie*
# Teilnahmen
[[1956]] - Franca Raimondi, "Aprite le finestre" 🇮🇹 (*dt. Öffnet die Fenster*) & Tonina Torielli, "Amami se vuoi" 🇮🇹 (*dt. Liebe mich, wenn du willst*) - Platz 2
[[1957]] - Nunzio Gallo, "Corde della mia chitarra" 🇮🇹 (*dt. Saiten auf meiner Gitarre*) - Platz 6
# Erfolge

# Austragungen
