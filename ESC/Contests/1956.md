# Grand Prix Eurovision de la Chanson Européenne 1956
### 📍Lugano, [[Schweiz]]
### 24. Mai 1956 
📺 Veranstalter: [[Schweiz|SSR SSG]] 

## Informationen 👨🏻‍🎤
1955 beschloss die European Broadcasting Union - die *EBU* - einen gesamt-europäischen Musikwettbewerb ähnlich des beliebten italienischen Sanremo-Festivals zu veranstalten. Der erste *Grand Prix* sollte bereits am darauffolgenden Jahr im schweizerischen Lugano stattfinden. 
7 EBU-Länder nehmen mit jeweils 2 Songs teil und der Wettbewerb wird weitestgehend im Radio übertragen. In 10 Ländern wird der *Grand Prix* allerdings auch bereits im Fernsehen live übertragen.

## Teilnehmer 🎙️
[[Belgien]], [[Deutschland]], [[Frankreich]], [[Italien]], [[Luxemburg]], [[Niederlande]], [[Schweiz]] 🏆

## Gewinnersong 🏆
**Lys Assia** - "Refrain" 🇫🇷
*dt. Refrain*

```qrcode
https://www.youtube.com/watch?v=IyqIPvOkiRk
```

Lys Assia sang ebenfalls den 2. Titel für die Schweiz, das deutsche Lied "Das alte Karussell".

## Deutsche Teilnahme 🇩🇪
1. **Walter Andreas Schwarz** - "Im Wartesaal zum großen Glück" 🇩🇪
```qrcode
https://www.youtube.com/watch?v=Ha0sKgDI8JA
```
3. **Freddy Quinn** - "So geht das jede Nacht" 🇩🇪
```qrcode
https://www.youtube.com/watch?v=eZIdJT1POUA
```
Beide Titel erreichten (wie alle Songs außer dem des Gewinners) den 2. Platz.